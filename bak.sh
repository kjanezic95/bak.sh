#!/bin/bash
# bak.sh
# Bash script for making .BAK backups. Do what you want to do with it.
# I am not liable for damage this script may do!
# Kristjan J.

verbose='false'

# LOG
log="/home/kristjan/bak.log"
echo "--------------------------------------------" >> $log
echo "START: $(date)" >> $log

# Verbose functions, for info output and logging.
function verboseOutput() {
# verboseOutput "Message" "Log"
	log="$2"
	echo "$1" >> $log
	if [ "$verbose" = "true" ]; then	
  		echo "$1"
	fi
}
function verboseCommand() {
# verboseCommand "Message" "Log"
  	log="$2"
	echo "$1" >> $log
	if [ "$verbose" = "true" ]; then	
  		echo "$1"
	fi
}

# Help
print_usage() {
  	printf "Usage: \n \
  	  -a All files \n \
	  -h Hidden files ( files starting with \'.\') \n \
	  -d Only directories \n \
	  -f Only files \n \
	  -m Show text-based user interface menu \n \
	  -c How many copies to keep. Use like \"-c 5\" for 5 copies. \n \
	     Example, files with bigger number are older files: \n \
  	     file.sh file.sh.BAK.1 file.sh.BAK.2 \n \
	  -t Add timestamp to backup files. Example: \n \
	     file.sh.BAK.1.[DODAJ DATE!]  \n \
	  -i Interactive mode in terminal \n \
	  -y Yes to all prompts. Example: Overwrite? (y/n) > \n \
	  -?|* Display this help\n "
}

# Flags
while getopts 'adfmc:vh?yi' flag; do
  	case "${flag}" in
    		a) all_flag='true'
		   verboseOutput "all_flag=$all_flag" "$log" ;;
    		h) hidden_flag='true'
		   verboseOutput "hidden_flag=$hidden_flag" "$log" ;;
    		d) dirs_flag='true'
		   verboseOutput "dirs_flag=$dirs_flag" "$log" ;;
    		f) files_flag='true'
		   verboseOutput "files_flag=$files_flag" "$log" ;;
    		m) menu_flag='true'
		   verboseOutput "menu_flag=$menu_flag" "$log" ;;
    		c) copies_flag='true'
		   verboseOutput "copies_flag=$copies_flag" "$log" ;;
    		t) timestamp_flag='true'
		   verboseOutput "timestamp_flag=$timestamp_flag" "$log" ;;
    		v) verbose='true'
		   verboseOutput "verbose=$verbose" "$log" ;; 
		y) yes_flag='true'
		   verboseOutput "yes_flag=$yes_flag" "$log" ;;
		i) interactive_flag='true'
		   verboseOutput "interactive_flag=$interactive_flag" "$log" ;;
    	      *|?) print_usage
             	   exit 1 ;;
  	esac
done

# Flag check
function flagCheck() {
# flagCheckIfOnlyOneTrue "flags for error message" "max count" $bool1 $bool2 $bool65
# flags for error message example: "-a -d -f"
# max count example: "2" (max 2 true flags or exit)
	counter=1
	list=""
	flagsForError=$1
	maxCount=$2
	for parameter in "$@"; do
		if [ "$parameter" = "true" ]; then
			if [ $counter -gt $maxCount ]; then
				verboseOutput "Flag error: These flags cannot be used together:\n$flagsForError" "$log"
				if [ "$verbose" = "false" ]; then 
					echo -e "Flag error: These flags cannot be used together:\n$flagsForError"
				fi
				exit 1
			fi
			let counter++
		fi
	done
}
# Call flag check
flagCheck "-f -d -a" "1" $files_flag $all_flag $dirs_flag

#-------------------------------------------------------------#

# VarsV
files=()
dir=$(pwd) && verboseOutput "PWD DIR: $dir" "$log" || verboseOutput "ERROR: PWD DIR: $dir" "$log"

# Parameters
for parameter in "$@"; do
	verboseOutput "parameter: $parameter" "$log"
done

#-------------------------------------------------------------#

function fileList() {
	verboseOutput "START: function fileList" "$log"
 	# Get file list
 	if   [ "$all_flag" = "true" ] && [ "$hidden_flag" = 'true' ]; then
  		files=($(ls -A $dir))
	elif [ "$all_flag" = "true" ]; then
  		files=($(ls $dir))
	elif [ "$dirs_flag" = 'true' ] && [ "$hidden_flag" = 'true' ]; then
		echo ".folders.BAK TBD"
  	elif [ "$dirs_flag" = 'true' ]; then
		for file in *; do	
			if [ -d "$file" ]; then
			files+=("$file")
			fi
		done
  		#files=($(ls -d */))
  	elif [ "$files_flag" = 'true' ] && [ "$hidden_flag" = 'true' ]; then
  		files=($(ls -p -A | grep -v /))
  	elif [ "$files_flag" = 'true' ]; then
  		files=($(ls -p | grep -v /))
	else
		echo -e "Something went wrong :( \nfunction fileList did not set any file. Check flags!" | tee -a /dev/tty $log 
  	fi

	# Filter
	# swp swo *~
  	
	# File list
	verboseOutput "START: for file in fileList -> bakFile \$file" "$log"
	
	# balk file by file
	for file in "${files[@]}"; do
		bakFile "$file"
	done
}

#-------------------------------------------------------------#

# Get parameters for bakParameters function, needed if giving file names to script
parameters=()
for parameter in "$@"; do
	verboseOutput "parameters+=($parameter)" "$log"
	#echo "param: $parameter"
	parameters+=($parameter)
done

# backup a file or file array
function bakFile() {
	parameters=$1
	#echo $1	
	#echo "${parameters[@]}"
	
	# CP function
	function bakCP() {
		# Ignores flag params like "-vy"
		if [[ $1 != -* ]] && [[ $1 != *.BAK ]]; then # <- bashism	
			cp -r "$1" "$1.BAK" >> $log 2>&1
			verboseOutput "function bakFile:bakCP: cp -r $1 $1.BAK" "$log"
			
			# Write out backed up files, if verbose mode if off	
			if [ "$verbose" = "false" ]; then
				echo "$1 -> $1.BAK"
			fi
		else
			verboseOutput "function bakFile:bakCP: skipping $1" "$log"
		fi
	}
	
	# Goes trough params and checks if .BAK is already present and offers confirmation.	
	for parameter in "${parameters[@]}"; do	
		verboseOutput "function bakFile parameter: $parameter" "$log"

		# If there are no existing .BAK files or the -y flag is enabled, disable overwrite prompt
		if (([ ! -f "$parameter.BAK" ] && [ ! -d "$parameter.BAK" ]) || [ "$yes_flag" = "true" ]); then
			bakCP "$parameter"

		# offer prompt if .BAK already present
		elif [ -f "$parameter.BAK" ] || [ -d "$parameter.BAK" ]; then
        		echo -n "Backup file exists. Overwrite? (y/n) > "
        		read response
        		if [ "$response" = "n" ]; then
            			echo "Skipping: $parameter."
			elif [ "$response" = "y" ]; then 
				bakCP "$parameter"
			else 
				# Add loop untill y or n, case/while?
				echo "Wrong input, skipping $parameter."
			fi
    		fi
	done
}

#-------------------------------------------------------------#

#Interactive mode
function interactiveMode {
	interactive="1"
	if [ "$interactive" = "1" ]; then
	    	response=""
	    	ls -a $dir
	    	echo -n "Enter name of a file [$filename] > "
	    	read response
		echo $response
		if ([ -f "$response" ] || [ -d "$response" ]); then
			echo "bakFile $response"
			bakFile "$response"
		else
			echo -e "Error in filename:\n$filename"
		fi
	fi	
}

#-------------------------------------------------------------#

# Script start

if [ "$all_flag" = "true" ] || [ "$files_flag" = "true" ] || [ "$dirs_flag" = "true" ]; then
# If there is a flag for any of all/files/dirs then get list of files
	fileList
elif [ "$interactive_flag" = "true" ]; then
	#Interactive mode
	interactiveMode
else
	verboseOutput "START: function: bakFile \$parameters" "$log"
	# Else bak parameters
	bakFile $parameters 
fi

# END time
echo "END: $(date)" >> $log

# sudo time -p ./bak.sh -ay
